

## **一、原理**

1. 增量编译Java原理：
   Android ClassLoader的findClass会从ClassLoader.pathList.dexElements[]数组里面按顺序查找dex加载class，所以可以Hook了ClassLoader.pathList.dexElements[]，将补丁的dex插入到数组的最前端，所以会优先查找到修改的类。

<img src="./picture/4.png" style="zoom:50%;" />

2. 增量编译res原理

   在应用启动调用Application#attachBaseContext时，此时插件工程的Application还未初始化，替换ActivityThread里面的mPackages和mResourcePackages的资源路径，从而到达替换资源的效果。

   <img src="./picture/5.png" style="zoom:50%;" />

## 二、打包流程

1. 全量打包流程

<img src="./picture/6.png" style="zoom:50%;" />

2. 增量打包流程

<img src="./picture/7.png" style="zoom:50%;" />