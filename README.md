# **AndroidBuilder**

**这个是可以快速增量编译的插件，秒级编译，免安装，提高开发效率。**

[**原理说明**](./doc/theory.md) 

## 一、使用方法

依赖的资源有
1. build_tool ：项目外部工具包
2. maven依赖：gradle插件和执行aar（aar已经在插件配置，无需配置），[已经编译好的下载地址https://gitee.com/wuyijun/AndroidBuilder/releases/1.0.0](https://gitee.com/wuyijun/AndroidBuilder/releases/1.0.0)
3. builder-studio-plugin.jar， ide插件，用于执行增量编译的jar，非必要

配置说明，可参考项目

1. build_tool 复制到项目根目录

2. 配置maven仓库路径，buildscript和allprojects都需要配置

   
   ```
   buildscript {
       repositories {
           maven {
               url uri("c:\\qiqi\\release\\maven")
           }
       }
   }
   ... ...
   allprojects {
       repositories {
           maven {
               url uri("c:\\qiqi\\release\\maven")
           }
       }
   }  
   ```

3. 配置依赖库

   ```
   dependencies {
       classpath "com.qiqi.builder:gradle:1.0.0"
   }
   ```

4. 配置参数

   ```
   subprojects {
       apply plugin: 'com.qiqi.builder'
       builder {
        	//配置application类名和attachBaseContext方法名，为了注入代码
           applicationName = 'com.qiqi.builder.TestApplication#attachBaseContext'
           //apk版本，非必须
           versionCode = 13000 
       }
   }
   ```

5. 点run全量编译

配置成功

![](./doc/picture/1.png)

运行成功

![](./doc/picture/2.png)

6. 增量编译，运行以下脚本


   my_gradle_hot.bat ： 增量编译java，kotlin

   my_gradle_hot_res.bat ： 增量编译res文件
  ```
   其他功能请查看代码
   ./builder-cli/src/main/java/com/qiqi/Main.java
   注意！！！
  ./build_tool/config.json  需要在这里配置kotlin sdk路径才能增量编译kotlin
  ```
7.  重新全量编译

    my_delete.bat ： 删除build/my，然后可以全量打包

8.  编译kotlin要配置kotlin的sdk

    ![](./doc/picture/kotlin.png)

9.  安装插件，非必要，可以使用bat代替

    ![](./doc/picture/plugin.png)

10.  有问题看log先

    ![](./doc/picture/3.png)

## 二、目录说明

1. [builder-lib] 公共代码

2. [builder-cli] 增量编译java、res代码工程，执行脚本如下：
   
   my_clear.bat ： 清除手机的增量dex和res
   
   my_delete.bat ： 删除build/my，然后可以全量打包
   
   my_gradle_hot.bat ： 增量编译java，kotlin
   
   my_gradle_hot_res.bat ： 增量编译res文件
   
   my_install_run.bat ： 清除手机的增量dex和res，然后安装apk
   
   my_reset.bat ： 清除手机的增量dex和res，然后push缓存的增量dex和res
   
3. [builder-studio-plugin] 简单的ide插件，执行脚本工程

4. [builder-runtime] 增量加载lib工程

5. [builder-gradle-plugin]  [buildsrc] gradle编译插件工程

6. [builder-sample-android] demo工程

7. [build_tool] 工具包





