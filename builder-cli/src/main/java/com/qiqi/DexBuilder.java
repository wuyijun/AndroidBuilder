package com.qiqi;


import com.qiqi.utils.BuildUtils;
import com.qiqi.utils.ClassPathUtil;
import com.qiqi.utils.CmdUtil;
import com.qiqi.utils.CollectUtil;
import com.qiqi.utils.FileScanHelper;
import com.qiqi.utils.FileUtil;
import com.qiqi.utils.Log;
import com.qiqi.utils.MD5Util;
import com.qiqi.utils.StringUtil;
import com.qiqi.utils.ZipUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

/**
 * 扫描java--》扫描缓存class--》增量编译class--》打包dex（dex\\cache\\classes）-->推倒sd卡--》重启app
 */
public class DexBuilder extends BaseBuilder {

    public static void main(String[] args) throws Exception {
        BuildUtils.initConfig();
        new DexBuilder().start();
    }

    public DexBuilder() {
        super(BuildUtils.getJavaInfoPath(), BuildUtils.getScanPathList()
                , BuildUtils.getIncreaseClassList(), BuildUtils.getExpelClassList());
    }

    public void start() throws Exception {
        long start = System.currentTimeMillis();
        preStart();
        if (mCompileList.size() > 0) {
            compile();
            classes2DexUseD8();
            pushDex2SD();
            Main.restart();
        }
        Log.i("dex执行时间：" + (float) (System.currentTimeMillis() - start) / 1000 + "秒");
    }

    @Override
    protected void scanFile(FileScanHelper helper, String path) {
        helper.scanJavaAndKotlin(path);
    }

    @Override
    protected boolean canIncrease(String path, String name) {
        return path.endsWith(File.separator + name + ".java");
    }

    @Override
    protected boolean canExpel(String path, String name) {
        return path.endsWith(File.separator + name + ".java");
    }

    public void compile() throws Exception {
        String classDestPath = BuildUtils.getBuildMyPath() + "\\dex\\classes";
        File classDestFile = new File(classDestPath);
        FileUtil.deleteDir(classDestFile);
        FileUtil.ensumeDir(classDestFile);

        processClassPath();

        Set<String> ktList = getList(".kt");
        if (!CollectUtil.isEmpty(ktList)) {
            compileKotlin();//需要先编译Kotlin，classpath需要kt的class
            Log.i("compile kotlin ok");
        }
        compileJava();
        Log.i("compile java ok");

    }

    Set<String> mClassPath = new HashSet<>();

    private void processClassPath() {
        List<String> allJarList = BuildUtils.getJarPathList();//文件jar list
        mClassPath.clear();
        mClassPath.addAll(allJarList);

        Set<String> ktList = getList(".kt");
        if (CollectUtil.isEmpty(ktList)) {
            return;
        }

        String jarOutPath = BuildUtils.getBuildMyPath() + "\\dex\\jar\\class";
        FileUtil.ensumeDir(jarOutPath);

        if (CollectUtil.isEmpty(mEidtCompileFileList)) {
            return;
        }
        Set<String> javaList = new HashSet<>();
        Set<String> javaFileList = new HashSet<>();
        //获取修改的类
        for (FileScanHelper.FileInfo info : mEidtCompileFileList) {
            if (info.path.endsWith(".java")) {
                Log.i("path:" + info.root);
                String name = info.path.substring(info.root.length(), info.path.lastIndexOf("."))
                        .replace("\\", "/");
                if (name.startsWith("/")) {
                    name = name.substring(1, name.length());
                }
                javaList.add(name);
                javaFileList.add(name.replace("/", "\\"));
            }
        }

        if (CollectUtil.isEmpty(javaList)) {
            return;
        }

        Log.i("javaList:" + javaList);

        //解压和删除class，防止编译冲突
        for (String jar : allJarList) {
            if (!jar.startsWith(BuildUtils.getConfigEntity().root_dir)) {
                continue;
            }
            String newJar = jar;
            if (jar.endsWith(".jar") && ClassPathUtil.checkHasFile(jar, javaList)) {
                Log.i("jar remove:" + jar);
                newJar = jarOutPath + "\\" + MD5Util.getMd5(jar);
                if (!FileUtil.dirExists(newJar)) {
                    ZipUtil.unZip(jar, newJar);
                }
                mClassPath.remove(jar);
                mClassPath.add(newJar);
            }
            if (FileUtil.dirExists(newJar)) {
                ClassPathUtil.fileRemoveClass(new File(newJar), javaFileList);
            }
        }
    }

    public void compileJava() throws Exception {
        Set<String> javaList = getList(".java");
        if (CollectUtil.isEmpty(javaList)) {
            return;
        }
        List<String> cmdArgs = new ArrayList<>();
        cmdArgs.add(BuildUtils.getJavacCmdPath());
        cmdArgs.add("-encoding");
        cmdArgs.add("UTF-8");
        cmdArgs.add("-g");
        cmdArgs.add("-target");
        cmdArgs.add("1.8");
        cmdArgs.add("-source");
        cmdArgs.add("1.8");
        cmdArgs.addAll(compileClassPath(javaList));
        CmdUtil.cmd(cmdArgs);
    }

    private Set<String> getList(String suffix) {
        Set<String> set = new HashSet<>();
        for (String s : mCompileList) {
            if (s.endsWith(suffix)) {
                set.add(s);
            }
        }
        return set;
    }

    public void compileKotlin() throws Exception {
        Set<String> ktList = getList(".kt");
        if (CollectUtil.isEmpty(ktList)) {
            return;
        }
        List<String> cmdArgs = new ArrayList<>();
//        cmdArgs.add("kotlinc.bat");
        cmdArgs.add(BuildUtils.getJavaCmdPath());
        cmdArgs.add("-cp");
        cmdArgs.add(BuildUtils.getKotlinPath() + "\\lib\\kotlin-compiler.jar");
        cmdArgs.add("org.jetbrains.kotlin.cli.jvm.K2JVMCompiler");
        cmdArgs.add("-jvm-target");
        cmdArgs.add("1.8");
        cmdArgs.add("-nowarn");
        cmdArgs.add("-no-stdlib ");

        cmdArgs.addAll(compileClassPath(mCompileList));
        CmdUtil.cmd(cmdArgs);
    }

    public List<String> compileClassPath(Set<String> javaLit) throws IOException {

        List<String> cmdArgs = new ArrayList<>();

        Set<String> classPath = new LinkedHashSet<>();

        //输出路径
        String classDestPath = BuildUtils.getBuildMyPath() + "\\dex\\classes";
        classPath.add(classDestPath);

        //引用编译的jar，class
        String androidJar = BuildUtils.getAndroidJarPath();
        classPath.add(androidJar);//android.jar

        String rClassDestPath = BuildUtils.getResourcesClassesPath();
        classPath.add(rClassDestPath);//r class

        classPath.addAll(mClassPath);

        cmdArgs.add("-cp");
        cmdArgs.add(joinClasspath(classPath));

        //输出路径
        cmdArgs.add("-d");
        cmdArgs.add(classDestPath);

        String src_list_txt = BuildUtils.getBuildMyPath() + "\\dex\\src_list.txt";
        FileUtil.deleteFile(new File(src_list_txt));
        FileUtil.writeFile(javaLit, src_list_txt);
        cmdArgs.add("@" + src_list_txt);

        return cmdArgs;
    }

    private String joinClasspath(Set<String> collection) {
        StringBuilder sb = new StringBuilder();

        boolean window = true;
        for (String s : collection) {
            if (!StringUtil.isEmpty(s) && (new File(s).exists())) {
                sb.append(s);
                if (window) {
                    sb.append(";");
                } else {
                    sb.append(":");
                }
            }
        }
        return sb.toString();
    }

    @Deprecated
    public void pack2Jar() {
        String destPath = BuildUtils.getBuildMyPath() + "\\dex";
        String classPath = BuildUtils.getBuildMyPath() + "\\dex\\classes";
        FileUtil.deleteFile(destPath);
        Log.i("jar classPath : " + classPath);
        List<String> cmdArgs = new ArrayList<>();
        cmdArgs.add(BuildUtils.getJarCmdPath());
        cmdArgs.add("-cvf");
        cmdArgs.add("patch.jar");
        cmdArgs.add("-C");
        cmdArgs.add(classPath);
        cmdArgs.add(".");
        CmdUtil.cmd(cmdArgs);

    }

    @Deprecated
    public void JarToDex() {
        Log.i(new File("").getAbsolutePath());
        String jarfile = new File("").getAbsolutePath() + "\\patch.jar";
        String dexOut = BuildUtils.getBuildMyPath() + "\\dex\\patch_dex.jar";
        FileUtil.deleteFile(dexOut);
        File dxFile = new File(BuildUtils.getDxCmdPath());
        List<String> dexargs = new ArrayList<>();
        dexargs.add(dxFile.getAbsolutePath());
        dexargs.add("--dex");
//        dexargs.add("--no-locals")
//        dexargs.add("--force-jumbo");
//         添加此标志以规避dx.jar对"java/"、"javax/"的检查
//        dexargs.add("--core-library")
        // disableDexMerger="${dex.disable.merger}"

        dexargs.add("--output=" + dexOut);
        dexargs.add(jarfile);

        CmdUtil.cmd(dexargs);
    }

    //d8
    public void classes2DexUseD8() {
        String jarPath = BuildUtils.getBuildMyPath() + "\\dex\\patch_dex_temp.jar";
        String dexOut = BuildUtils.getBuildMyPath() + "\\dex\\patch_dex.jar";
        String classPath = BuildUtils.getBuildMyPath() + "\\dex\\classes";

        File file = new File(classPath);
        if (file.isDirectory()) {
            long time = System.currentTimeMillis();
            ZipUtil.zip(jarPath, classPath);
            Log.i("class zip time：" + (System.currentTimeMillis() - time));
        }

        List<String> cmd = new ArrayList<>();
        cmd.add(BuildUtils.getD8CmdPath());
        cmd.add("--debug");
        cmd.add("--min-api");
        cmd.add("26");//26解决java8特性问题
        cmd.add("--lib");
        cmd.add(BuildUtils.getAndroidJarPath());
        cmd.add("--output");
        cmd.add(dexOut);
        cmd.add(jarPath);
//        cmd.add("-JXms1024M");
//        cmd.add("-JXmx2048M");
        CmdUtil.cmd(cmd);
    }

    //    E:\develop\software\Android\sdk\build-tools\25.0.3\dx.bat --dex  --output=.\patch_dex.jar .\build
    @Deprecated
    public void classes2Dex() {
        String dexOut = BuildUtils.getBuildMyPath() + "\\dex\\patch_dex.jar";
        String classPath = BuildUtils.getBuildMyPath() + "\\dex\\classes";
        Log.i("jar classPath : " + classPath);
        List<String> cmdArgs = new ArrayList<>();
        cmdArgs.add(BuildUtils.getDxCmdPath());
        cmdArgs.add("--dex");
        cmdArgs.add("--output=" + dexOut);
        cmdArgs.add(classPath);
        CmdUtil.cmd(cmdArgs);
    }

    public static void pushDex2SD() {
        String dexPath = BuildUtils.getBuildMyPath() + "\\dex\\patch_dex.jar";

        List<String> cmdArgs = new ArrayList<>();
        cmdArgs.add(BuildUtils.getAdbCmdPath());
        cmdArgs.add("push");
        cmdArgs.add(dexPath);
        cmdArgs.add(BuildUtils.getExternalCacheDir());

        CmdUtil.cmd(cmdArgs);
    }

}
